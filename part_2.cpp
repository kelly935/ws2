class Scholar:
{
    public:
        Scholar(string name, string id);
        string getName();
        string getID();

    private:
        string name;
        string id;

};

//Todo Lecturer class needs name variable and name getter
//Todo There are also numerous syntax errors in the code for part 1 and it will not compile