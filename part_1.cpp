class Student {
    private:
        string name;
        string id;

    public: 
        //! Public Variable - should be private with getter/setter
        vector<Activity> myActivities;
        Student(string name, string id);
        string getName();
        string getID();
        
        //! Functionality from another class - everything should be 
        //!     modular (ie Activities class should be doing this)
        //! Class Definitions and Implimentations should be separated into .h and .cpp files
        //!     Inline functions are an exception
        void doActivity() { 
             // for each problem in each of myActivities

             //? Syntax Error: n not defined in student class
             myActivies.at(n).solution = "Solution problem n";
        }
           
};

class Lecturer {
    private:
        string id;

    public:
        //! Public Variable - should be private with getter/setter
        vector<student> class;
        Lecturer(string id);
        string getName();
        string getID();
        
        //! Functionality from another class - everything should be 
        //!     modular (ie Activities class should be doing this)
        //! Class Definitions and Implimentations should be separated into .h and .cpp files
        //!     Inline functions are an exception
        void runActivity() {
            Activity nextActivity;
            nextActivity.problems.pushback("question1......");
            nextActivity.problems.pushback("question2......");
            nextActivity.problems.pushback("question3......");

            //? Syntax Error: For loop is just comment, not declared
            // for each student, s,  in the class do the following
                s.myActivities.pushback(nextActivity);
                s.doActivity();

                // check the solutions by accessing student.myActivities.solutions
                // remove the activities from the student's list
                s.myActivities.clear();
        }
};     

//! Class with only variables and no functions - could just be made into a struct
class Activity {
     public:
          //! Public Variables - should be private with getter/setter
          string name;  // name of the activity e.g. "Workshop 1"
          vector<string> problems;
          vector<string> solutions;
}

int main(void) {
    string lecturerName = "Cheryl";
    Lecturer addsLecturer("a123456");
    Student addsStudent1("Jasmyn", "a55667788");

    //? Syntax Error: need a function to do the push back also .addsLecturer has no member .class
    addsLecturer.class.pushback(addsStudent1);
    // create rest of Students and add to Lecturer's class

    addsLecturer.runActivity();
}